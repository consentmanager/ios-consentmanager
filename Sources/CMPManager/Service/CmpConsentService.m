//
// Created by Skander Ben Abdelmalak on 20.11.21.
//

#import "CmpConsentService.h"
#import "CMPConsentV2Parser.h"
#import "CMPDataStorageConsentManagerUserDefaults.h"
#import "CMPDataStoragePrivateUserDefaults.h"
#import "CMPDataStorageV1UserDefaults.h"
#import "CMPDataStorageV2UserDefaults.h"
#import "CmpConsentDto.h"
#import "CmpUtils.h"
#import "Logger.h"

@implementation CmpConsentService

static NSString *TAG = @"[Cmp]ConsentService";
static NSString *CMP_CHECK_API_RESPONSE = @"CMP_CHECKAPI_RESPONSE";
static NSString *CMP_CHECK_API_LASTUPDATE = @"CMP_CHECKAPI_LASTUPDATE";

+ (instancetype)sharedInstance {
    static CmpConsentService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      sharedInstance = [[CmpConsentService alloc] init];
    });
    return sharedInstance;
}

- (CmpConsentDto *)getConsentDto {
    @try {
        CmpConsentDto *consentDto = [CmpRepository fetchCmpUserConsent];
        return consentDto;
    }
    @catch (id exception) {
        _onErrorListener(CmpConsentDataReadWriteError, @"Error while reading Consent Data");
        return [CmpConsentDto createEmpty];
    }
}

- (bool)saveConsentDto:(CmpConsentDto *)consentDto {
    // validate consentString
    if (![CmpConsentDto isValid:consentDto]) {
        [Logger error:TAG:@"Consent String is not valid"];
        _onErrorListener(CmpConsentDataReadWriteError, @"Error while saving Consent Data");
        return NO;
    }
    @try {
        [CmpConsentService parseConsentManagerString:consentDto.cmpStringBase64Encoded];
        [CmpRepository saveCmpUserConsent:consentDto];
    }
    @catch (id anException) {
        _onErrorListener(CmpConsentDataReadWriteError, @"Error while saving Consent Data");
        [Logger error:TAG:anException];
        return NO;
    }
    [Logger debug:TAG:[NSString stringWithFormat:@"written V2User defaults: %@", [CMPDataStorageV2UserDefaults description]]];
    [Logger info:TAG:@"User Accepted a new Consent"];
    return YES;
}

- (NSString *)getCmpStringBase64Encoded {
    //  NSString *cmpApiKey = [CmpConsentLocalRepository getCmpApiKey];
    NSString *cmpKey = [CMPDataStorageConsentManagerUserDefaults consentString];
    return cmpKey;
}

- (void)reset {
    [CmpRepository removeCmpConsent];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:CMP_CHECK_API_RESPONSE];
    [defaults removeObjectForKey:CMP_CHECK_API_LASTUPDATE];
    [CMPDataStorageV1UserDefaults clearContents];
    [CMPDataStorageV2UserDefaults clearContents];
    [CMPDataStoragePrivateUserDefaults clearContents];
    [CMPDataStorageConsentManagerUserDefaults clearContents];
}

+ (void)userImportedConsent:(NSString *)data {
    [Logger debug:TAG:[NSString stringWithFormat:@"consent passed to import: %@", data]];
}

+ (void)consentLayerOpened {
    // TODO refactor to dedicated state Repository
    [CMPDataStorageV1UserDefaults setCmpPresent:YES];
}

#pragma mark helper
+ (void)proceedConsentString:(NSString *)consentS {
    [Logger debug:TAG:[NSString stringWithFormat:@"V2 String detected"]];
    [CMPDataStorageV2UserDefaults setTcString:consentS];
    (void)[[CMPConsentV2Parser alloc] init:consentS];
}

+ (void)parseConsentManagerString:(NSString *)consentString {
    [Logger debug:TAG:[NSString stringWithFormat:@"%@:parse ConsentApiKey: %@", TAG, consentString]];
    [CMPDataStorageV1UserDefaults setCmpPresent:NO];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [CMPDataStoragePrivateUserDefaults setLastRequested:[dateFormatter stringFromDate:[NSDate date]]];
    [CMPDataStoragePrivateUserDefaults setNeedsAcceptance:NO];

    @try {
        if (consentString != NULL && consentString.length > 0 && ![consentString isEqualToString:@"null"] && ![consentString isEqualToString:@"nil"] && ![consentString isEqualToString:@""]) {
            [CMPDataStorageConsentManagerUserDefaults setConsentString:consentString];
            NSString *base64Decoded = [CmpUtils binaryStringConsentFrom:consentString];
            [Logger debug:TAG:[NSString stringWithFormat:@"decoded base64 consent String: %@", base64Decoded]];
            NSArray *splits = [base64Decoded componentsSeparatedByString:@"#"];

            if (splits.count > 3) {
                [Logger debug:TAG:[NSString stringWithFormat:@"ConsentManager String detected: %@", splits[0]]];
                [self proceedConsentString:splits[0]];
                [self proceedConsentManagerValues:splits];
            } else {
                [CMPDataStorageV1UserDefaults clearContents];
                [CMPDataStorageV2UserDefaults clearContents];
            }
        } else {
            [CMPDataStorageV1UserDefaults clearContents];
            [CMPDataStorageV2UserDefaults clearContents];
        }
    }
    @catch (NSException *e) {
        [CMPDataStorageV1UserDefaults clearContents];
        [CMPDataStorageV2UserDefaults clearContents];
    }
}

+ (void)proceedConsentManagerValues:(NSArray *)splits {
    if (splits.count > 1) {
        [CMPDataStorageConsentManagerUserDefaults setParsedPurposeConsents:splits[1]];
        [Logger debug:TAG:[NSString stringWithFormat:@"ParsedPurposeConsents:%@", splits[1]]];
    }
    if (splits.count > 2) {
        [CMPDataStorageConsentManagerUserDefaults setParsedVendorConsents:splits[2]];
        [Logger debug:TAG:[NSString stringWithFormat:@"ParsedVendorConsents:%@", splits[2]]];
    }
    if (splits.count > 3) {
        [CMPDataStorageConsentManagerUserDefaults setUsPrivacyString:splits[3]];
        [Logger debug:TAG:[NSString stringWithFormat:@"ParsedUSPrivacy:%@", splits[3]]];
    }
    if (splits.count > 4) {
        [CMPDataStorageConsentManagerUserDefaults setGoogleACString:splits[4]];
        [Logger debug:TAG:[NSString stringWithFormat:@"GoogleACString:%@", splits[4]]];
    }
}

+ (BOOL)validConsent {
    return [CMPDataStorageConsentManagerUserDefaults consentString] != nil || [CMPDataStorageConsentManagerUserDefaults consentString].length > 0;
}

+ (BOOL)hasConsent {
    return [CmpRepository hasConsent];
}

+ (void)saveCheckApiResponse:(BOOL)checkResponse {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:checkResponse forKey:CMP_CHECK_API_RESPONSE];
    [defaults setObject:[NSDate date] forKey:CMP_CHECK_API_LASTUPDATE];
    [defaults synchronize];
}

+ (NSDate *)lastCheckApiUpdate {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:CMP_CHECK_API_LASTUPDATE];
}

@end
