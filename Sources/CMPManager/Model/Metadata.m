//
//  Metadata.m
//  CmpSdk
//
//  Created by Skander Ben Abdelmalak on 23.02.23.
//

#import "Metadata.h"
#import <Foundation/Foundation.h>

@implementation Metadata

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.value forKey:@"value"];
    [aCoder encodeObject:self.type forKey:@"type"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.value = [aDecoder decodeObjectForKey:@"value"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
    }
    return self;
}

- (void)convertValueType {
    if ([self.type isEqualToString:@"int"]) {
        self.value = @([self.value intValue]);
    } else if ([self.type isEqualToString:@"string"]) {
        self.value = [NSString stringWithString:self.value];
    }
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

@end
