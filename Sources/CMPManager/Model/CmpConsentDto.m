//
// Created by Skander Ben Abdelmalak on 30.11.21.
//

#import "CmpConsentDto.h"
#import "Logger.h"
#import "Metadata.h"

@implementation CmpConsentDto {
}
static NSString *TAG = @"[Cmp]ConsentDto";

#pragma mark constructors

+ (CmpConsentDto *)fromJSON:(NSDictionary *)jsonData {

    CmpConsentDto *user_consent = [[CmpConsentDto alloc] init];
    NSArray *enabledPurpose = [jsonData[@"purposeConsents"] allKeys];
    NSArray *enabledVendors = [jsonData[@"vendorConsents"] allKeys];
    NSArray *allPurposes = [jsonData[@"purposesList"] valueForKey:@"id"];
    NSArray *allVendors = [jsonData[@"vendorsList"] valueForKey:@"id"];
    NSArray *googleVendorList = [jsonData[@"googleVendorConsents"] allKeys];

    user_consent.cmpStringBase64Encoded = jsonData[@"cmpString"];
    user_consent.consentString = jsonData[@"consentstring"];
    user_consent.googleACString = jsonData[@"addtlConsent"];
    user_consent.gdprApplies = [jsonData[@"gdprApplies"] boolValue];
    user_consent.regulation = jsonData[@"regulation"];
    user_consent.regulationKey = jsonData[@"regulationKey"];
    user_consent.tcfCompliant = [jsonData[@"tcfcompliant"] boolValue];
    user_consent.tcfVersion = jsonData[@"tcfversion"];
    user_consent.uspString = jsonData[@"uspstring"];
    user_consent.googleVendorList = googleVendorList;
    user_consent.purposeConsentList = [jsonData[@"purposeConsents"] allKeys];
    user_consent.allPurposeList = allPurposes;
    user_consent.allVendorList = allVendors;
    user_consent.enabledPurposes = enabledPurpose;
    user_consent.enabledVendors = enabledVendors;
    user_consent.metadata = [jsonData[@"metadata"] allObjects];
    user_consent.hasGlobalScope = [jsonData[@"hasGlobalScope"] boolValue];
    user_consent.userChoiceExists = [jsonData[@"userChoiceExists"] boolValue];
    user_consent.lastButtonEvent = jsonData[@"lastButtonEvent"];

    if (![CmpConsentDto isValid:user_consent]) {
        NSLog(@"%@:error during instantiating Consent Data from Json, Consent not valid: %@", TAG, user_consent.description);
    }

    return user_consent;
}

+ (CmpConsentDto *)createEmpty {
    CmpConsentDto *emptyDto = [[CmpConsentDto alloc] init];
    emptyDto.cmpStringBase64Encoded = @"";
    emptyDto.consentString = @"";
    emptyDto.googleACString = @"";
    emptyDto.gdprApplies = NO;
    emptyDto.regulation = [NSNumber numberWithInt:-1];
    emptyDto.regulationKey = @"";
    emptyDto.tcfCompliant = NO;
    emptyDto.tcfVersion = [NSNumber numberWithInt:-1];
    emptyDto.uspString = @"";
    emptyDto.googleVendorList = @[];
    emptyDto.purposeConsentList = @[];
    emptyDto.allPurposeList = @[];
    emptyDto.allVendorList = @[];
    emptyDto.enabledPurposes = @[];
    emptyDto.enabledVendors = @[];
    emptyDto.metadata = @[];
    emptyDto.hasGlobalScope = NO;
    emptyDto.userChoiceExists = NO;
    emptyDto.lastButtonEvent = [NSNumber numberWithInt:-1];
    return emptyDto;
}

// check more constructors, which apply to business logic.

#pragma mark getter and business Logic

// check other business logic maybe also just internally logic for adding just one vendor etc.
// would like be .. addVendor
// inside of service addVendor Method which gets CmpUserConsent Object from repository
// then calling addVendor and then saving it back to repository
// improved performance if there would be predefined saving cases, like just save vendors to the new value
// CmpUserConsent would just be the data transfer
// service need to handle instantiation of Dto for different cases and command repository what it needs to do.

- (BOOL)hasVendor:(NSString *)vendor {
    [Logger debug:TAG:[NSString stringWithFormat:@"Check for vendor %@ on List: %@", vendor, _enabledVendors]];
    if ([_regulation isEqualToNumber:@0]) {
        return YES;
    }
    return [_enabledVendors containsObject:vendor] ? YES : NO;
}

- (BOOL)hasPurpose:(NSString *)purpose {
    [Logger debug:TAG:[NSString stringWithFormat:@"Check for purpose %@ on List:", _enabledPurposes]];
    if ([_regulation isEqualToNumber:@0]) {
        return YES;
    }

    return [_enabledPurposes containsObject:purpose] ? YES : NO;
}

- (NSArray *)getEnabledVendorList {
    return _enabledVendors;
}

- (NSArray *)getEnabledPurposeList {
    return _enabledPurposes;
}

- (NSArray *)getGoogleVendorList {
    return _googleVendorList;
}

- (NSString *)getUsPrivacy {
    return _uspString;
}

//-(void)getMetadata {
//    for (NSDictionary *metadata in _metadata) {
//            NSString *name = metadata[@"name"];
//            id value = metadata[@"value"];
//            NSString *type = metadata[@"type"];
//
//            NSLog(@"Name: %@, Value: %@, Type: %@", name, value, type);
//        }
//}
- (NSArray *)getDisabledVendorList {
    NSMutableSet *disabledVendors = [NSMutableSet setWithArray:self.allVendorList];
    [disabledVendors minusSet:[NSSet setWithArray:self.enabledVendors]];
    return [disabledVendors allObjects];
}
- (NSArray *)getDisabledPurposeList {
    NSMutableSet *disabledPurposes = [NSMutableSet setWithArray:self.allPurposeList];
    [disabledPurposes minusSet:[NSSet setWithArray:self.enabledPurposes]];
    return [disabledPurposes allObjects];
}
- (NSString *)getCmpStringBase64Encoded {
    return _consentString;
}

- (NSArray *)getAllVendorList {
    return _allVendorList;
}
- (NSString *)getAllVendors {
    return [_allVendorList componentsJoinedByString:@"_"];
}
- (NSArray *)getAllPurposeList {
    return _allPurposeList;
}
- (NSString *)getAllPurposes {
    return [_allVendorList componentsJoinedByString:@"_"];
}
#pragma mark Logging

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"  self.cmpString=%@", _cmpStringBase64Encoded];
    [description appendFormat:@"  self.consentString=%@", _consentString];

    [description appendFormat:@", self.regulation=%@", _regulation];
    [description appendFormat:@", self.gdprApplies=%d", _gdprApplies];
    [description appendFormat:@", self.regulationKey=%@", _regulationKey];
    [description appendFormat:@", self.tcfVersion=%@", _tcfVersion];
    [description appendFormat:@", self.tcfCompliant=%d", _tcfCompliant];
    [description appendFormat:@", self.uspString=%@", _uspString];
    [description appendFormat:@", self.googleVendorList=%@", [_googleVendorList componentsJoinedByString:@","]];
    [description appendFormat:@", self.purposeConsentList=%@", [_purposeConsentList componentsJoinedByString:@","]];
    [description appendFormat:@", self.purposeList=%@", [_allPurposeList componentsJoinedByString:@","]];
    [description appendFormat:@", self.vendorList=%@", [_allVendorList componentsJoinedByString:@","]];
    [description appendFormat:@", self.agreedPurposes=%@", [_enabledPurposes componentsJoinedByString:@","]];
    [description appendFormat:@", self.agreedVendors=%@", [_enabledVendors componentsJoinedByString:@","]];
    [description appendFormat:@", self.metadata=%@", [_metadata description]];
    [description appendFormat:@", self.hasGlobalScope=%d", _hasGlobalScope];
    [description appendFormat:@", self.userChoiceExists=%d", _userChoiceExists];
    [description appendString:@">"];
    return description;
}

+ (bool)isValid:(CmpConsentDto *)dto {

    if (dto == NULL)
        return NO;
    return YES;
}

#pragma mark Decoding/Encoding

- (void)encodeWithCoder:(NSCoder *)encoder {

    //  user_consent.allPurposeList = allPurposes;
    [encoder encodeObject:self.cmpStringBase64Encoded forKey:@"cmpString"];
    [encoder encodeObject:self.consentString forKey:@"consentString"];
    [encoder encodeObject:self.googleACString forKey:@"addtlConsent"];
    [encoder encodeObject:self.regulation forKey:@"regulation"];
    [encoder encodeObject:self.regulationKey forKey:@"regulationKey"];
    [encoder encodeObject:@(self.gdprApplies) forKey:@"gdprApplies"];
    [encoder encodeObject:self.tcfVersion forKey:@"tcfVersion"];
    [encoder encodeObject:@(self.tcfCompliant) forKey:@"tcfCompliant"];
    [encoder encodeObject:self.uspString forKey:@"uspString"];
    [encoder encodeObject:self.googleVendorList forKey:@"googleVendorList"];
    [encoder encodeObject:self.purposeConsentList forKey:@"purposeConsentList"];
    [encoder encodeObject:self.allPurposeList forKey:@"purposeList"];
    [encoder encodeObject:self.allVendorList forKey:@"vendorList"];
    [encoder encodeObject:self.enabledPurposes forKey:@"purposeConsents"];
    [encoder encodeObject:self.enabledVendors forKey:@"vendorConsents"];
    [encoder encodeObject:self.metadata forKey:@"metadata"];
    [encoder encodeObject:@(self.hasGlobalScope) forKey:@"hasGlobalScope"];
    [encoder encodeObject:@(self.userChoiceExists) forKey:@"userChoiceExists"];
    [encoder encodeObject:self.lastButtonEvent forKey:@"lastButtonEvent"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super init])) {
        // decode properties, other class vars
        self.cmpStringBase64Encoded = [decoder decodeObjectForKey:@"cmpString"];
        self.consentString = [decoder decodeObjectForKey:@"consentString"];
        self.googleACString = [decoder decodeObjectForKey:@"addtlConsent"];
        self.regulation = [decoder decodeObjectForKey:@"regulation"];
        self.regulationKey = [decoder decodeObjectForKey:@"regulationKey"];
        self.gdprApplies = [[decoder decodeObjectForKey:@"gdprApplies"] boolValue];
        self.tcfVersion = [decoder decodeObjectOfClass:[NSNumber class] forKey:@"tcfVersion"];
        self.tcfCompliant = [[decoder decodeObjectForKey:@"tcfCompliant"] boolValue];
        self.uspString = [decoder decodeObjectForKey:@"uspString"];
        self.googleVendorList = [decoder decodeObjectForKey:@"googleVendorList"];
        self.purposeConsentList = [decoder decodeObjectForKey:@"purposeConsentList"];
        self.allPurposeList = [decoder decodeObjectForKey:@"purposeList"];
        self.allVendorList = [decoder decodeObjectForKey:@"vendorList"];
        self.enabledPurposes = [decoder decodeObjectForKey:@"purposeConsents"];
        self.enabledVendors = [decoder decodeObjectForKey:@"vendorConsents"];
        self.metadata = [decoder decodeObjectForKey:@"metadata"];
        self.hasGlobalScope = [[decoder decodeObjectForKey:@"hasGlobalScope"] boolValue];
        self.userChoiceExists = [[decoder decodeObjectForKey:@"userChoiceExists"] boolValue];
        self.lastButtonEvent = [decoder decodeObjectOfClass:[NSNumber class] forKey:@"lastButtonEvent"];
    }

    if (![CmpConsentDto isValid:self]) {
        NSLog(@"%@:error during instantiating Consent Data from User defaults, Consent not valid: %@", TAG, self.cmpStringBase64Encoded);
    }

    return self;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}
@end
