//
//  CmpRepository.swift
//  CmpSdk
//
//  Created by Skander Ben Abdelmalak on 24.10.22.
//

import Foundation

// IABTCF Keys
private let CMP_SDK_ID = "IABTCF_CmpSdkID"
private let CMP_SDK_VERSION = "IABTCF_CmpSdkVersion"
private let POLICY_VERSION = "IABTCF_PolicyVersion"
private let GDPR_APPLIES = "IABTCF_gdprApplies"
private let PUBLISHER_CC = "IABTCF_PublisherCC"
private let TC_STRING = "IABTCF_TCString"
private let VENDOR_CONSENTS = "IABTCF_VendorConsents"
private let VENDOR_LEGITIMATE_INTERESTS = "IABTCF_VendorLegitimateInterests"
private let PURPOSE_CONSENTS = "IABTCF_PurposeConsents"
private let PURPOSE_LEGITIMATE_INTERESTS = "IABTCF_PurposeLegitimateInterests"
private let SPECIAL_FEATURES_OPT_INS = "IABTCF_SpecialFeaturesOptIns"
private let PUBLISHER_RESTRICTIONS = "IABTCF_PublisherRestrictions%d" // %d = Purpose ID
private let PUBLISHER_CONSENT = "IABTCF_PublisherConsent"
private let PUBLISHER_LEGITIMATE_INTERESTS = "IABTCF_PublisherLegitimateInterests"
private let PUBLISHER_CUSTOM_PURPOSES_CONSENTS = "IABTCF_PublisherCustomPurposesConsents"
private let PUBLISHER_CUSTOM_PURPOSES_LEGITIMATE_INTERESTS = "IABTCF_PublisherCustomPurposesLegitimateInterests"
private let PURPOSE_ONE_TREATMENT = "IABTCF_PurposeOneTreatment"
private let USE_NONE_STANDARD_STACKS = "IABTCF_UseNoneStandardStacks"
private let CMP_REGULATION_STATUS = "IABTCF_RegulationStatus"

// Private keys
private var CONSENT_STRING = "CMConsent_ConsentString"
private var CMP_USER_CONSENT_KEY = "cmp_user_consent"

let defaults = UserDefaults.standard
//let allowedClasses = Set
//var dto = fetchCmpUserConsent()
class CmpRepository {

//    class func getCmpConsentDto() -> CmpConsentDto? {
//        let defaults = UserDefaults.standard
//        let encodedObject = defaults.object(forKey: CMP_USER_CONSENT_KEY) as? Data
//        let allowedClasses = Set<AnyHashable>([CmpConsentDto.self, NSString.self, NSNumber.self, [AnyHashable].self])
//        var cmpUserConsentDto: CmpConsentDto? = nil
//        do {
//            if let encodedObject {
//                cmpUserConsentDto = try NSKeyedUnarchiver.unarchivedObject(ofClasses: allowedClasses, from: encodedObject) as? CmpConsentDto
//            }
//        } catch {
//            print("\(error.localizedDescription)")
//        }
//        return cmpUserConsentDto
//    }

//    class func saveCmpConsentDto(_ userConsent: CmpConsentDto?) {
//        var encodedObject: Data? = nil
//        do {
//            if let userConsent {
//                encodedObject = try NSKeyedArchiver.archivedData(withRootObject: userConsent, requiringSecureCoding: false)
//            }
//        } catch {
//            NSException.raise("Error during saving User Consent values", format: "Error Message: %@", error)
//        }
//        CMPDataStorageConsentManagerUserDefaults.consentString = userConsent?.cmpApiKey
//        var defaults = UserDefaults.standard
//        defaults.set(encodedObject, forKey: CMP_USER_CONSENT_KEY)
//        defaults.synchronize()
//    }
}
