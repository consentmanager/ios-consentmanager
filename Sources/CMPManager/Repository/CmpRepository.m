//
// Created by Skander Ben Abdelmalak on 22.11.21.
//

#import "CmpRepository.h"
#import "CMPDataStorageConsentManagerUserDefaults.h"
#import "CmpConsentDto.h"
#import "Logger.h"

static NSString *CONSENT_STRING = @"CMConsent_ConsentString";
static NSString *CMP_USER_CONSENT_KEY = @"cmp_user_consent";
@implementation CmpRepository {
}

+ (void)saveCmpUserConsent:(CmpConsentDto *)userConsent {
    NSError *error;
    NSData
        *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:userConsent requiringSecureCoding:NO error:&error];
    if (error) {
        [NSException raise:@"Error during saving User Consent values" format:@"Error Message: %@", error];
    }
    [CMPDataStorageConsentManagerUserDefaults setConsentString:userConsent.cmpStringBase64Encoded];
    [Logger debug:@"Saving Consent":userConsent.description];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:CMP_USER_CONSENT_KEY];
    [defaults synchronize];
}

+ (CmpConsentDto *)fetchCmpUserConsent {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:CMP_USER_CONSENT_KEY];
    NSSet *allowedClasses =
        [NSSet setWithObjects:[CmpConsentDto class], [Metadata class], [NSMutableDictionary class], [NSString class], [NSNumber class], [NSArray class], nil];

    NSError *error = nil;
    if (encodedObject == nil) {
        return [CmpConsentDto createEmpty];
    }
    CmpConsentDto *cmpUserConsentDto =
        [NSKeyedUnarchiver unarchivedObjectOfClasses:allowedClasses
                                            fromData:encodedObject
                                               error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        [CmpRepository removeCmpConsent];
        [NSException raise:@"Error during reading User Consent values" format:@"Error Message: %@", error];
    }
    return cmpUserConsentDto;
}

+ (NSString *)getCmpStringBase64Encoded {
    CmpConsentDto *dto = self.fetchCmpUserConsent;
    return dto.cmpStringBase64Encoded;
}

+ (void)removeCmpConsent {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:CMP_USER_CONSENT_KEY];
    [defaults removeObjectForKey:CONSENT_STRING];
}

+ (BOOL)hasConsent {
    BOOL consent = [[self fetchCmpUserConsent] userChoiceExists];
    return consent;
}

@end
