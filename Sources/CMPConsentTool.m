//
//  CMPConsentTool.m
//  GDPA
//
//

#import "CMPConsentTool.h"
#import "ATTrackingHelper.h"
#import "CMPConsentV2Parser.h"
#import "CMPDataStorageConsentManagerUserDefaults.h"
#import "CMPDataStoragePrivateUserDefaults.h"
#import "CMPDataStorageV1UserDefaults.h"
#import "CMPDataStorageV2UserDefaults.h"
#import "CmpConsentDto.h"
#import "CmpLayerViewController.h"
#import "CmpUtils.h"
#import "Logger.h"

@interface CMPConsentTool ()<CmpLayerViewControllerDelegate, CmpPlaceholderDelegate>
- (void)openCmpLayer;
@end

@implementation CMPConsentTool

@synthesize closeListener;
@synthesize openListener;
@synthesize errorListener;

/// @deprecated
@synthesize customOpenListener;

@synthesize onCMPNotOpenedListener;
@synthesize onCmpButtonClickedCallback;

static NSString *TAG = @"[Cmp]ConsentTool";

- (CmpConfig *)cmpConfig {
  return _cmpConfig;
}

///
/// - Parameter callback
- (void)onCMPNotOpenedListener:(void (^)(void))callback {
  onCMPNotOpenedListener = callback;
}
///
/// - Parameter listener
/// - Returns:
- (id)closeListener:(void (^)(void))listener {
  closeListener = listener;
  return self;
}
///
/// - Parameter listener
- (void)openListener:(void (^)(void))listener {
  openListener = listener;
}
///
/// - Parameter listener
- (void)customOpenListener:(void (^)(CMPSettings *settings))listener {
  customOpenListener = listener;
}
///
/// - Parameter errorListener: Callback to add action if an error occurred. The block should have the following signature: `void (^)(CmpErrorType, NSString *)`
/// - Returns:
- (id)errorListener:(void (^)(CmpErrorType, NSString *))errorListener {
  errorListener = errorListener;
  return self;
}
///
/// - Parameter listener
- (void)buttonClickedListener:(void (^)(CmpButtonEvent))listener {
  onCmpButtonClickedCallback = listener;
}
#pragma mark old API
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController {
  return [self init:domain addId:cmpId addAppName:appName addLanguage:language addViewController:viewController autoupdate:TRUE];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener {
  return [self init:domain addId:cmpId addAppName:appName addLanguage:language addViewController:viewController autoupdate:TRUE addOpenListener:openListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addCloseListener:(void (^)(
	void))closeListener {
  return [self init:domain addId:cmpId addAppName:appName addLanguage:language addViewController:viewController autoupdate:TRUE addCloseListener:closeListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)      init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener
addCloseListener:(void (^)(void))closeListener {
  return [self init:domain addId:cmpId addAppName:appName addLanguage:language addViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:closeListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)      init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener
addCloseListener:(void (^)(void))closeListener {
  return [self init:domain addId:cmpId addAppName:appName addLanguage:language addIDFA:idfa addViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:closeListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addCloseListener:(void (^)(
	void))closeListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addCloseListener:closeListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)      init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addCloseListener:(void (^)(void))closeListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:closeListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)      init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addCloseListener:(void (^)(void))closeListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:closeListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter errorListener: Callback to add action if an error occurred. The block should have the following signature: `void (^)(CmpErrorType, NSString *)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController addErrorListener:(void (^)(
	CmpErrorType,
	NSString *))errorListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:nil addCloseListener:nil addErrorListener:errorListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController addOpenListener:(void (^)(void))openListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:openListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController addCloseListener:(void (^)(void))closeListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addCloseListener:closeListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController addOpenListener:(void (^)(void))openListener addCloseListener:(void (^)(
	void))closeListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:closeListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate {
  return [self init:config withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:nil];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter notOpenListener
/// - Returns: ``CMPConsentTool`` instance
- (id)                             init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener addCloseListener:(void (^)(void))closeListener addOnCMPNotOpenedListener:(void (^)(void))notOpenListener {
  return [self init:config withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:closeListener addOnCMPNotOpenedListener:notOpenListener addErrorListener:nil];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener {
  return [self init:config withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:nil];
}

///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addCloseListener:(void (^)(
	void))closeListener {
  return [self init:config withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:closeListener];
}

/// init with following parameters
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`. <#openListener description#>
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

/// init with following parameters
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`. <#openListener description#>
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener
		 addCloseListener:(void (^)(void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

/// init with following parameters
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addOnCMPNotOpenedListener:(void (^)(
	void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:nil addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

/// init with following parameters
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`. <#openListener description#>
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
		 addCloseListener:(void (^)(void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController addOpenListener:(void (^)(void))openListener addOnCMPNotOpenedListener:(void (^)(
	void))onCMPNotOpenedListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController addCloseListener:(void (^)(void))closeListener addOnCMPNotOpenedListener:(void (^)(
	void))onCMPNotOpenedListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addCloseListener:(void (^)(
	void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(CmpConfig *)config withViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener
		 addCloseListener:(void (^)(void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:openListener addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Parameter backendVerification: If enabled the consentmanager server will be requested to check if the consent layer needs to be opened
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(CmpConfig *)config withViewController:(UIViewController *)viewController addOpenListener:(void (^)(
	void))openListener
		 addCloseListener:(void (^)(void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener
   addBackendVerification:(Boolean)backendVerification {

  self.cmpConfig = [CmpConfig self];
  self.viewController = viewController;
  self.closeListener = closeListener;
  self.openListener = openListener;
  self.onCMPNotOpenedListener = onCMPNotOpenedListener;

  [self checkAttStatus];
  if (backendVerification) {
	[self proceedConsentUpdate];
  } else {
	[self checkAndProceedConsentUpdate];
  }

  [[NSNotificationCenter defaultCenter] addObserver:self.viewController
										   selector:@selector(onApplicationDidBecomeActive:)
											   name:@"NSApplicationDidBecomeActiveNotification"
											 object:nil];
  return self;
}

/// <#Description#>
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`. <#openListener description#>
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

/// <#Description#>
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOnCMPNotOpenedListener:(void (^)(
	void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOnCMPNotOpenedListener:(void (^)(
	void))onCMPNotOpenedListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addCloseListener:(void (^)(
	void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController addCloseListener:(void (^)(
	void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addCloseListener:(void (^)(
	void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:TRUE addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)      init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addCloseListener:(void (^)(void))closeListener {
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:closeListener addOnCMPNotOpenedListener:nil];
}

///
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter errorListener: Callback to add action if an error occurred. The block should have the following signature: `void (^)(CmpErrorType, NSString *)`
/// - Returns: ``CMPConsentTool`` instance
- (id)      init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addCloseListener:(void (^)(void))closeListener
addErrorListener:(void (^)(CmpErrorType, NSString *))errorListener {
  return [self       init:[CmpConfig self]
	   withViewController:viewController
			   autoupdate:autoupdate
		  addOpenListener:openListener
		 addCloseListener:closeListener
addOnCMPNotOpenedListener:nil
		 addErrorListener:errorListener];
}

//----------------------- PART Initialize with autoupdate (8) -------------------------------------

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOnCMPNotOpenedListener:(void (^)(
	void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:nil addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addCloseListener:(void (^)(
	void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}
///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
		 addCloseListener:(void (^)(void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:closeListener addOnCMPNotOpenedListener:onCMPNotOpenedListener];
}

//----------------------- PART Initialize with IDFA and autoupdate (8) -------------------------------------

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:nil addOnCMPNotOpenedListener:nil];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:openListener addCloseListener:nil addOnCMPNotOpenedListener:nil];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter idfa: IDFA `String`
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (id)init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addIDFA:(NSString *)idfa addViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addCloseListener:(void (^)(
	void))closeListener {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  [CmpConfig setIDFA:idfa];
  return [self init:[CmpConfig self] withViewController:viewController autoupdate:autoupdate addOpenListener:nil addCloseListener:closeListener addOnCMPNotOpenedListener:nil];
}

///
/// - Parameter domain: `String` cmp domain
/// - Parameter cmpId: `String` cmp id
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Parameter backendVerification: If enabled the consentmanager server will be requested to check if the consent layer needs to be opened
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(NSString *)domain addId:(NSString *)cmpId addAppName:(NSString *)appName addLanguage:(NSString *)language addViewController:(UIViewController *)viewController addCloseListener:(void (^)(
	void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener
  addBackendeVerification:(Boolean)backendVerification {
  [CmpConfig setValues:domain addCodeId:cmpId addAppName:appName addLanguage:language];
  self.viewController = viewController;
  self.closeListener = closeListener;
  self.openListener = openListener;
  self.onCMPNotOpenedListener = onCMPNotOpenedListener;
  [self checkAttStatus];
  if (backendVerification) {
	[self proceedConsentUpdate];
  } else {
	[self checkAndProceedConsentUpdate];
  }

  [[NSNotificationCenter defaultCenter] addObserver:self.viewController
										   selector:@selector(onApplicationDidBecomeActive:)
											   name:@"NSApplicationDidBecomeActiveNotification"
											 object:nil];
  return self;
}

/// Main constructor
/// - Parameter config ``CmpConfig`` instance
/// - Parameter viewController: `UViewController` instance
/// - Parameter autoupdate: Performs an automatic check to determine if the user needs to provide consent.
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
/// - Parameter errorListener: Callback to add action if an error occurred. The block should have the following signature: `void (^)(CmpErrorType, NSString *)`
/// - Returns: ``CMPConsentTool`` instance
- (id)               init:(CmpConfig *)config withViewController:(UIViewController *)viewController autoupdate:(BOOL)autoupdate addOpenListener:(void (^)(
	void))openListener
		 addCloseListener:(void (^)(void))closeListener
addOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener
		 addErrorListener:(void (^)(CmpErrorType, NSString *))errorListener {
  self.cmpConfig = config;
  self.viewController = viewController;
  self.closeListener = closeListener;
  self.openListener = openListener;
  self.errorListener = errorListener;

  self.onCMPNotOpenedListener = onCMPNotOpenedListener;
  [self registerUserDefaults];
  self.consentServiceInstance = [CmpConsentService sharedInstance];
  [self.consentServiceInstance setOnOpenListener:openListener];
  [self.consentServiceInstance setOnCloseListener:closeListener];
  [self.consentServiceInstance setOnErrorListener:errorListener];
  [self.consentServiceInstance setOnCmpNotOpenedListener:onCMPNotOpenedListener];

  [self checkAttStatus];
  if (autoupdate) {
	[self checkAndProceedConsentUpdate];
	[[NSNotificationCenter defaultCenter] addObserver:self.viewController
											 selector:@selector(onApplicationDidBecomeActive:)
												 name:@"NSApplicationDidBecomeActiveNotification"
											   object:nil];
  }
  return self;
}

#pragma mark constructor
///
/// - Parameter domain: `String` cmp domain
/// - Parameter codeId
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
/// - Parameter viewController: `UViewController` instance
/// - Returns: ``CMPConsentTool`` instance
- (instancetype)initWithDomain:(NSString *)domain
						codeId:(NSString *)codeId
					   appName:(NSString *)appName
					  language:(NSString *)language
				viewController:(UIViewController *)viewController {
  self = [super init];

  if (self) {
	self.consentServiceInstance = [CmpConsentService sharedInstance];
	[CmpConfig setValues:domain
			   addCodeId:codeId
			  addAppName:appName
			 addLanguage:language];
	_viewController = viewController;
  }
  return self;
}

///
/// - Parameter cmpConfig
/// - Parameter viewController: `UViewController` instance
/// - Returns: ``CMPConsentTool`` instance
- (instancetype)initWithCmpConfig:(CmpConfig *)cmpConfig
				   viewController:(UIViewController *)viewController {
  self = [super init];
  if (self) {
	self.consentServiceInstance = [CmpConsentService sharedInstance];
	self.cmpConfig = cmpConfig;
	_viewController = viewController;
  }
  return self;
}

///
/// - Parameter closeListener: Callback which will be called when the consent layer is closed: The block should have the following signature: `void (^)(void)`
/// - Returns: ``CMPConsentTool`` instance
- (instancetype)withCloseListener:(void (^)(void))closeListener {
  self.closeListener = closeListener;
  [self.consentServiceInstance setOnCloseListener:closeListener];
  return self;
}

///
/// - Parameter openListener: Callback which will be called when the consent layer is opened: The block should have the following signature: `void (^)(void)`.
/// - Returns: ``CMPConsentTool`` instance
- (instancetype)withOpenListener:(void (^)(void))openListener {
  self.openListener = openListener;
  [self.consentServiceInstance setOnOpenListener:openListener];
  return self;
}


/// Add a Cmp error callback to get notification when an error occurs
/// - Parameters:
///     - errorListener: Callback
- (instancetype)withErrorListener:(void (^)(CmpErrorType, NSString *))errorListener {
  self.errorListener = errorListener;
  [self.consentServiceInstance setOnErrorListener:errorListener];
  return self;
}


/// Add a Cmp not opened callback to get notification when consent layer is not opening
/// - Parameter listener: callback
- (instancetype)withCustomOpenListener:(void (^)(CMPSettings *))listener {
  self.customOpenListener = listener;
  return self;
}


/// Add a Cmp not opened callback to get notification when consent layer is not opening
/// Most common reason is that the user already gave a consent
/// - Parameter onCMPNotOpenedListener: Callback which will be called when the consent layer is `not opened`: The block should have the following signature: `void (^)(void)`
- (instancetype)withOnCMPNotOpenedListener:(void (^)(void))onCMPNotOpenedListener {
  self.onCMPNotOpenedListener = onCMPNotOpenedListener;
  [self.consentServiceInstance
	  setOnCmpNotOpenedListener:onCMPNotOpenedListener];
  return self;
}


/// Add a Cmp Button clicked callback to get notification which button was clicked in the consent layer
/// - Parameter onCmpButtonClickedCallback Callback
- (instancetype)withOnCmpButtonClickedCallback:(void (^)(CmpButtonEvent))onCmpButtonClickedCallback {
  [self.consentServiceInstance
	  setOnButtonClickedCallback:onCmpButtonClickedCallback];
  self.onCmpButtonClickedCallback = onCmpButtonClickedCallback;
  return self;
}

///
/// - Returns: ``CMPConsentTool``
- (instancetype)initialize {
  NSLog(@"initializing");
  [self checkAttStatus];
  return self;
}

///
- (void)openCmpConsentToolView {
  [self openCmpLayer];
}

///
/// - Parameter close_listener Close Listener Callback
- (void)openCmpConsentToolView:(void (^)(void))close_listener {
  [self openCmpLayer];
}

///
- (void)verifyOpenCmpLayer {
  /// initialize ViewController
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.errorListener = [self errorListener];

  /// @attention just Open View is the switch for the VC whether it verifies to open Layer or not
  consentToolVC.justOpenView = NO;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

/// @brief Open Cmp Layer - with force option. This function opens the Layer without verify if it needs to be opened
/// @example if User explicitly wants to open layer to change conset
- (void)openCmpLayer {
  // TODO should be deleted when local data is refactored
  if ([CmpConfig isValid]) {
	[CMPSettings setConsentString:[CMPDataStorageConsentManagerUserDefaults consentString]];
  } else {
	[Logger error:TAG :@"CmpConfig is invalid"];
  }
  // initialize ViewController
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];

  consentToolVC.errorListener = [self errorListener];
  /// @attention just Open View is the switch for the VC whether it verifies to open Layer or not
  consentToolVC.justOpenView = YES;
  consentToolVC.timedOut = NO;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

#pragma mark CMPConsentToolViewController delegate
/// Delegate Method to get callback from ViewController with the desired consent String
/// - Parameter cmpLayerViewController ViewController
/// - Parameter consentString consentString
- (void)didReceivedConsentString:(CmpLayerViewController *)cmpLayerViewController consentString:(NSString *)consentString {
  [Logger warning:TAG :@"Use of deprecated method"];
  [self parseConsentManagerString:consentString];
  if (self.closeListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.closeListener();
	});
  }
}

/// Delegate Method to get callback from ViewController with the desired consent String
/// - Parameter cmpLayerViewController ViewController
/// - Parameter cmpUserConsentDto Consent Object
- (void)didReceivedConsentDto:(CmpLayerViewController *)cmpLayerViewController :(CmpConsentDto *)cmpUserConsentDto {
  [Logger debug:@"Consent accepted:" :[NSString stringWithFormat:@"%@", cmpUserConsentDto.description]];
  [CmpConsentService saveCheckApiResponse:NO];
  [_consentServiceInstance saveConsentDto:cmpUserConsentDto];
  cmpLayerViewController.isOpen = NO;
  cmpLayerViewController.isMessageSent = YES;
  [cmpLayerViewController willMoveToParentViewController:nil];
  [cmpLayerViewController removeFromParentViewController];
  [cmpLayerViewController dismissViewControllerAnimated:YES completion:nil];
  if (cmpLayerViewController.isOpen) {
	[self handleCloseEvent];
  } else {
	[self handleLayerNotOpenedEvent];
  }
  [self handleButtonClickedEvent:[self buttonEventFromInt:cmpUserConsentDto.lastButtonEvent.intValue]];
}

/// Preloading the WebView to increase UX, delegate to get feedback from ViewController when WebView is finished
/// - Parameter cmpLayerViewController ViewController
- (void)didFinishedLoading:(CmpLayerViewController *)cmpLayerViewController {

  if (!cmpLayerViewController.isOpen && !cmpLayerViewController.isMessageSent) {
	cmpLayerViewController.isOpen = YES;
	[CmpConsentService consentLayerOpened];
	[self handleLayerOpenedEvent];
	[self.viewController presentViewController:cmpLayerViewController animated:YES completion:nil];
  } else {
	[Logger warning:TAG :@"The Consent Layer has Problems to open the View. Please try again later"];
	[self handleErrorEvent:CmpTimeoutError message:@"The Consent Layer has Problems to open the View. Please try again later"];
  }
}

///
/// - Parameter cmpLayerViewController
- (void)cancelConsentLayer:(CmpLayerViewController *)cmpLayerViewController {
  [cmpLayerViewController dismissViewControllerAnimated:YES completion:nil];
  [cmpLayerViewController reset];
}

#pragma mark handling callback Events

/// Handle Event callback on Close
- (void)handleCloseEvent {
  if (closeListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.closeListener();
	});
  }
}

/// Handle Event callback on Consent Layer opened
- (void)handleLayerOpenedEvent {
  if (self.openListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.openListener();
	});
  }

  if (self.customOpenListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.customOpenListener([CMPSettings self]);
	});
	return;
  }
}

/// Handle Event callback on Consent Layer *NOT* opened
- (void)handleLayerNotOpenedEvent {
  if (onCMPNotOpenedListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  NSLog(@"SOMETHING");
	  self.onCMPNotOpenedListener();
	});
  }
}

/// Handle Event callback on Button pressed to close the Consent layer
/// - Parameter event Type of Button event
- (void)handleButtonClickedEvent:(CmpButtonEvent)event {
  if (self.onCmpButtonClickedCallback) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.onCmpButtonClickedCallback(event);
	});
  }
}

///
/// - Parameter intValue
/// - Returns:
- (CmpButtonEvent)buttonEventFromInt:(NSInteger)intValue {
  if (intValue >= CmpButtonEventUnknown && intValue <= CmpButtonEventClose) {
	return (CmpButtonEvent)intValue;
  } else {
	return CmpButtonEventUnknown;
  }
}

///
/// - Parameter errorType
/// - Parameter errorMessage
- (void)handleErrorEvent:(CmpErrorType)errorType message:(NSString *)errorMessage {
  if (errorListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.errorListener(errorType, errorMessage);
	});
  }
}

#pragma mark helper methods

///
/// - Parameter consentString
- (void)parseConsentManagerString:(NSString *)consentString {
  [Logger debug:TAG :[NSString stringWithFormat:@"parse Consent: %@", consentString]];
  [CMPDataStorageV1UserDefaults setCmpPresent:NO];

  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];
  [CMPDataStoragePrivateUserDefaults setNeedsAcceptance:NO];

  @try {
	if (consentString != NULL && consentString.length > 0 && ![consentString isEqualToString:@"null"]
		&& ![consentString isEqualToString:@"nil"] && ![consentString isEqualToString:@""]) {
	  [CMPDataStorageConsentManagerUserDefaults setConsentString:consentString];
	  NSString *base64Decoded = [CmpUtils binaryStringConsentFrom:consentString];
	  [Logger debug:TAG :[NSString stringWithFormat:@"decoded base64: %@", base64Decoded]];
	  NSArray *splits = [base64Decoded componentsSeparatedByString:@"#"];

	  if (splits.count > 3) {
		[Logger debug:TAG :[NSString stringWithFormat:@"%@, ConsentManager String detected: %@", TAG, splits[0]]];
		[self proceedConsentString:splits[0]];
		[self proceedConsentManagerValues:splits];
	  } else {
		[CMPDataStorageV1UserDefaults clearContents];
		[CMPDataStorageV2UserDefaults clearContents];
	  }
	} else {
	  [CMPDataStorageV1UserDefaults clearContents];
	  [CMPDataStorageV2UserDefaults clearContents];
	}
  }
  @catch (NSException *e) {
	[CMPDataStorageV1UserDefaults clearContents];
	[CMPDataStorageV2UserDefaults clearContents];
  }
}

///
/// - Parameter cmpData
/// - Returns:
- (BOOL)importCmpString:(NSString *)cmpData {
  [CmpConsentService userImportedConsent:cmpData];
  [self parseConsentManagerString:cmpData];
  return YES;
}

///
/// - Returns:
+ (NSString *)exportCmpString {
  return [CMPDataStorageConsentManagerUserDefaults consentString];
}

/// Verify Consent and gives a valid consent String
- (void)checkAndOpenConsentLayer {
  [self verifyOpenCmpLayer];
}

///
/// - Parameter consentS
- (void)proceedConsentString:(NSString *)consentS {
  [CMPDataStorageV2UserDefaults setTcString:consentS];
  (void)[[CMPConsentV2Parser alloc] init:consentS];
}

///
/// - Parameter splits
- (void)proceedConsentManagerValues:(NSArray *)splits {
  if (splits.count > 1) {
	[CMPDataStorageConsentManagerUserDefaults setParsedPurposeConsents:splits[1]];
	[Logger debug:TAG :[NSString stringWithFormat:@"ParsedPurposeConsents:%@", splits[1]]];
  }
  if (splits.count > 2) {
	[CMPDataStorageConsentManagerUserDefaults setParsedVendorConsents:splits[2]];
	[Logger debug:TAG :[NSString stringWithFormat:@"ParsedVendorConsents:%@", splits[2]]];
  }
  if (splits.count > 3) {
	[CMPDataStorageConsentManagerUserDefaults setUsPrivacyString:splits[3]];
	[Logger debug:TAG :[NSString stringWithFormat:@"ParsedUSPrivacy:%@", splits[3]]];
  }
  if (splits.count > 4) {
	[CMPDataStorageConsentManagerUserDefaults setGoogleACString:splits[4]];
	[Logger debug:TAG :[NSString stringWithFormat:@"GoogleACString:%@", splits[4]]];
  }
}

///
/// - Returns:
- (NSString *)getVendorsString {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getAllVendors];
}

///
/// - Returns:
- (NSString *)getEnabledVendors {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [[consentDto getEnabledVendorList] componentsJoinedByString:@"_"];
}

///
/// - Returns:
- (NSArray *)getEnabledVendorList {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getEnabledVendorList];
}

///
/// - Returns:
- (NSString *)getEnabledPurposes {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [[consentDto getEnabledPurposeList] componentsJoinedByString:@"_"];
}

///
/// - Returns:
- (NSString *)getPurposesString {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [[consentDto getEnabledVendorList] componentsJoinedByString:@"_"];
}

///
/// - Returns:
- (NSArray *)getEnabledPurposeList {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getEnabledPurposeList];
}

///
/// - Returns:
- (NSArray *)getDisabledVendorList {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return consentDto.getDisabledVendorList;
}

///
/// - Returns:
- (NSString *)getDisabledVendors {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto.getDisabledVendorList componentsJoinedByString:@"_"];
}

///
/// - Returns:
- (NSArray *)getDisabledPurposeList {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return consentDto.getDisabledPurposeList;
}

///
/// - Returns:
- (NSString *)getDisabledPurposes {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto.getDisabledPurposeList componentsJoinedByString:@"_"];
}

///
/// - Returns:
- (NSArray *)getAllPurposesList {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto allPurposeList];
}

///
/// - Returns:
- (NSString *)getAllPurposes {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getAllPurposes];
}

///
/// - Returns:
- (NSArray *)getAllVendorList {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getAllVendorList];
}

///
/// - Returns:
- (NSString *)getAllVendors {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getAllVendors];
}

///
/// - Returns:
- (NSString *)getUSPrivacyString {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto getUsPrivacy];
}

///
/// - Returns:
- (NSString *)getGoogleACString {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto googleACString];
}

///
/// - Parameter vendorId
/// - Parameter isIABVendor
/// - Returns:
- (BOOL)hasVendorConsent:(NSString *)vendorId vendorIsV1orV2:(BOOL)isIABVendor {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto hasVendor:vendorId];
}

/// Checks if the vendor ID is enabled based on the user consent.
///
/// - Parameters:
///     - vendorId: vendor id
/// - Returns: TRUE if the user has given consent to the specified vendor, FALSE otherwise.
- (BOOL)hasVendorConsent:(NSString *)vendorId {
    CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
    return [consentDto hasVendor:vendorId];
}

///
/// - Parameter purposeId
/// - Parameter isIABPurpose deprecated: boolean if the purpose id is an IAB purpose
/// - Returns: BOOL if the user gives consent about the purpose
- (BOOL)hasPurposeConsent:(NSString *)purposeId purposeIsV1orV2:(BOOL)isIABPurpose {
  CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
  return [consentDto hasPurpose:purposeId];
}

/// Checks if the purpose ID is enabled based on the user consent.
///
/// - Parameters:
///     - purposeId: purpose id
/// - Returns: TRUE if the user has given consent to the specified vendor, FALSE otherwise.
- (BOOL)hasPurposeConsent:(NSString *)purposeId {
    CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
    return [consentDto hasPurpose:purposeId];
}

///
/// - Parameter purposeId purpose id
/// - Parameter vendorId vendor id
/// - Returns: BOOL if the user gives consent about the purpose
- (BOOL)hasPurposeConsent:(int)purposeId forVendor:(int)vendorId {
  NSNumber *purposeIdInt = @(purposeId);
  PublisherRestriction *pr = [CMPDataStorageV2UserDefaults publisherRestriction:purposeIdInt];
  return [pr hasVendor:vendorId];
}

#pragma mark instance Methods

///
/// - Parameter notification
- (void)onApplicationDidBecomeActive:(NSNotification *)notification {
  [self checkAndProceedConsentUpdate];
}

///
- (void)checkAttStatus {
  if (@available(iOS 14.0, *)) {
	if ([CmpConfig getAutoAppleTracking]) { //@available(iOS 13.0, *)
	  [ATTrackingHelper requestATTPermission];
	}
  }
}

///
- (void)checkAndProceedConsentUpdate {
  if ([self needsServerUpdate]) {
	[self verifyOpenCmpLayer];
  } else {
	[self handleLayerNotOpenedEvent];
	[Logger info:TAG :@"No update needed. Server was already requested today and Consent was given."];
  }
}

///
- (void)proceedConsentUpdate {
  // check if ATT is activated
  [self verifyOpenCmpLayer];
}

///
/// - Returns:
- (BOOL)needsServerUpdate {
  return ![self calledThisDay] || ![self isConsentValid];
}

///
/// - Returns:
- (BOOL)hasConsent {
  return [CmpConsentService hasConsent];
}

///
/// - Returns:
- (BOOL)isConsentValid {
  return [CmpConsentService validConsent];
}

///
/// - Returns:
- (BOOL)needsConsentAcceptance {
  return [self needsAcceptance];
}

///
/// - Returns:
- (CMPServerResponse *)proceedServerRequest {
  return [CmpUtils getServerResponse:errorListener withConsent:[CMPDataStorageConsentManagerUserDefaults consentString]];
}

///
/// - Parameter cmpServerResponse
- (void)proceedConsentUpdate:(CMPServerResponse *)cmpServerResponse {
  [self proceedConsentUpdate:cmpServerResponse withOpening:TRUE];
}

///
/// - Parameter cmpServerResponse
/// - Parameter opening
- (void)proceedConsentUpdate:(CMPServerResponse *)cmpServerResponse withOpening:(BOOL)opening {
  switch ([cmpServerResponse.status intValue]) {
	case 0:return;
	case 1:
	  if (opening) {
		[CMPDataStoragePrivateUserDefaults setNeedsAcceptance:TRUE];
		[self verifyOpenCmpLayer];
	  }
	  return;
	default:[self handleErrorEvent:CmpNetworkError message:cmpServerResponse.message];
	  break;
  }
}

///
/// - Returns:
- (BOOL)calledThisDay {
  NSString *last = [self getCalledLast];
  if (last) {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSString *now = [dateFormatter stringFromDate:[NSDate date]];
	return [now isEqualToString:last];
  }
  return FALSE;
}

///
/// - Returns:
- (BOOL)needsAcceptance {
  const Boolean needsAcceptance = [CMPDataStoragePrivateUserDefaults needsAcceptance];
  return needsAcceptance;
}

///
/// - Returns:
- (NSString *)getCalledLast {
  return [CMPDataStoragePrivateUserDefaults lastRequested];
}

///
+ (void)reset {
  [CmpConsentService.sharedInstance reset];
}

///
/// - Parameter onFinish
- (void)rejectAll:(void (^)(void))onFinish {
  // initialize ViewController
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.onFinish = onFinish;
  consentToolVC.errorListener = [self errorListener];

  /// @attention just Open View is the switch for the VC whether it verifies to open Layer or not
  consentToolVC.justOpenView = NO;
  consentToolVC.rejectAll = YES;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

///
/// - Parameter onFinish
- (void)acceptAll:(void (^)(void))onFinish {
  // initialize ViewController
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.onFinish = onFinish;
  consentToolVC.errorListener = [self errorListener];

  /// @attention just Open View is the switch for the VC whether it verifies to open Layer or not
  consentToolVC.justOpenView = NO;
  consentToolVC.rejectAll = NO;
  consentToolVC.acceptAll = YES;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

///
/// - Parameter onCmpLayerOpen
- (void)check:(void (^)(void))onCmpLayerOpen {
  /// set response to false
  [CmpConsentService saveCheckApiResponse:NO];
  /// initialize ViewController
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.onCheckApiResponse = onCmpLayerOpen;
  consentToolVC.errorListener = [self errorListener];
  [consentToolVC initWebView];
}

///
/// - Parameter onCmpLayerOpen
/// - Parameter isCached
- (void)check:(void (^)(void))onCmpLayerOpen isCached:(BOOL)isCached {
  // if called today give cached response
  if (isCached) {
	if ([self didCallCheckApiToday]) {
	  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	  BOOL checkAPIOpenLayer = [defaults boolForKey:@"CMP_CHECKAPI_RESPONSE"];
	  // based on saved value call OpenLayer Signal
	  if (checkAPIOpenLayer) {
		dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
		dispatch_async(queue, ^{
		  onCmpLayerOpen();
		});
	  }
	  return;
	}
  }

  /// set response to false
  [CmpConsentService saveCheckApiResponse:NO];

  /// initialize ViewController
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.onCheckApiResponse = onCmpLayerOpen;
  consentToolVC.errorListener = [self errorListener];
  [consentToolVC initWebView];
}

///
/// - Returns:
- (BOOL)didCallCheckApiToday {
  // Load the date and time of the last update
  NSDate *lastUpdate = [CmpConsentService lastCheckApiUpdate];

  // Get the current date and time
  NSDate *now = [NSDate date];

  // Calculate the time interval between the last update and now
  NSTimeInterval interval = [now timeIntervalSinceDate:lastUpdate];

  // If the last update was within 24 hours (86400 seconds), return true
  return (interval < 86400);
}

///
/// - Parameter vendors
/// - Parameter onFinish
- (void)enableVendorList:(NSArray *)vendors onFinish:(void (^)(void))onFinish {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.onFinish = onFinish;
  consentToolVC.acceptAll = YES;
  consentToolVC.delegate = self;

  consentToolVC.additionalUrlParameters =
	  [NSString stringWithFormat:@"%@%@", @"&cmpsetvendors=", [vendors componentsJoinedByString:@"_"]];

  [consentToolVC initWebView];
}

///
/// - Parameter vendors
/// - Parameter onFinish
- (void)disableVendorList:(NSArray *)vendors onFinish:(void (^)(void))onFinish {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.onFinish = onFinish;
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.rejectAll = YES;
  consentToolVC.delegate = self;

  consentToolVC.additionalUrlParameters =
	  [NSString stringWithFormat:@"%@%@", @"&cmpsetvendors=", [vendors componentsJoinedByString:@"_"]];

  [consentToolVC initWebView];
}

///
/// - Parameter purposes
/// - Parameter onFinish
- (void)enablePurposeList:(NSArray *)purposes onFinish:(void (^)(void))onFinish {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.onFinish = onFinish;
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.acceptAll = YES;
  consentToolVC.delegate = self;

  consentToolVC.additionalUrlParameters =
	  [@"&cmpsetpurposes=" stringByAppendingString:[purposes componentsJoinedByString:@"_"]];

  [consentToolVC initWebView];
}

///
/// - Parameter purposes
/// - Parameter onFinish
- (void)disablePurposeList:(NSArray *)purposes onFinish:(void (^)(void))onFinish {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.onFinish = onFinish;
  consentToolVC.rejectAll = YES;
  consentToolVC.delegate = self;

  consentToolVC.additionalUrlParameters =
	  [@"&cmpsetpurposes=" stringByAppendingString:[purposes componentsJoinedByString:@"_"]];

  [consentToolVC initWebView];
}

///
/// - Parameter vendors
- (void)enableVendorList:(NSArray *)vendors {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.additionalUrlParameters =
	  [NSString stringWithFormat:@"%@%@", @"&cmpsetvendors=", [vendors componentsJoinedByString:@"_"]];
  consentToolVC.acceptAll = YES;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

///
/// - Parameter vendors
- (void)disableVendorList:(NSArray *)vendors {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.additionalUrlParameters =
	  [NSString stringWithFormat:@"%@%@", @"&cmpsetvendors=", [vendors componentsJoinedByString:@"_"]];
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.rejectAll = YES;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

///
/// - Parameter purposes
- (void)enablePurposeList:(NSArray *)purposes {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.additionalUrlParameters =
	  [@"&cmpsetpurposes=" stringByAppendingString:[purposes componentsJoinedByString:@"_"]];
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.acceptAll = YES;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

///
/// - Parameter purposes
- (void)disablePurposeList:(NSArray *)purposes {
  CmpLayerViewController *consentToolVC = [[CmpLayerViewController alloc] init];
  consentToolVC.additionalUrlParameters =
	  [@"&cmpsetpurposes=" stringByAppendingString:[purposes componentsJoinedByString:@"_"]];
  consentToolVC.errorListener = [self errorListener];
  consentToolVC.rejectAll = YES;
  consentToolVC.delegate = self;
  [consentToolVC initWebView];
}

/// Gets consent String
///
/// - Returns: consent String
- (NSString *)getConsentString {
    CmpConsentDto *consentDto = _consentServiceInstance.getConsentDto;
    return [consentDto consentString];
}

///
- (void)registerUserDefaults {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults registerDefaults:@{
	  @"IABConsent_CMPRequest": @"",
	  @"IABConsent_CMPAccepted": @YES,

  }];
}

#pragma mark - Placeholder functions

///
/// - Parameter frame
/// - Parameter placeholderParams
/// - Returns:
- (CmpPlaceholderView *)createPlaceholder:(CGRect)frame :(CmpPlaceholderParams *)placeholderParams {
  CmpPlaceholderView *view = [[CmpPlaceholderView alloc] initWithPlaceholderParams:placeholderParams];
  view.delegate = self;

  return [view createPreview:frame];
}

///
/// - Parameter placeholderView UIView
/// - Parameter consent_string consentString
- (void)receivedConsentString:(CmpPlaceholderView *)placeholderView :(NSString *)consent_string {
  [Logger debug:TAG :@"Received Consent of Placeholder"];
  [self parseConsentManagerString:consent_string];
  if (self.closeListener) {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
	  self.closeListener();
	});
  }
  if ([placeholderView.vendorDelegate respondsToSelector:@selector(vendorAccepted:)]) {
	[placeholderView.vendorDelegate vendorAccepted:placeholderView];
  }
}
@end
