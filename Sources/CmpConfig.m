//
//  CmpConfig.m
//  GDPR
//

#import "CmpConfig.h"
#import "Logger.h"
#import <AppTrackingTransparency/AppTrackingTransparency.h>

NSString *const ConsentToolURLUnformatted = @"https://%@/delivery/appjson.php?id=%@&name=%@&consent=%@&idfa=%@&l=%@";

@interface CmpConfig ()
+ (NSInteger)getAttStatus:(NSUInteger)status;
@end
@implementation CmpConfig
static NSString *codeId = nil;
static NSString *appName = nil;
static NSString *domain = nil;
static NSString *language = nil;
static UIColor *backgroundColor = nil;
static BOOL attActive = FALSE;
static NSInteger verboseLevel = 0;
static NSInteger *attStatus = 0;
static BOOL skipToCustomizePage = FALSE;
static CGRect customLayout;
static long timeout = 6000;
static NSString *designId = nil;
static BOOL hasCustomLayout = FALSE;
static NSInteger modalTransitionStyle = UIModalPresentationOverFullScreen;

API_AVAILABLE(ios(14))
static ATTrackingManager *atTrackingManager = nil;
NSString *idfa = nil;

///
/// - Parameter domain: `String` cmp domain
/// - Parameter addId
/// - Parameter appName: `String` App name
/// - Parameter language: `String` language for the consentlayer
+ (void)setValues:(NSString *)domain addCodeId:(NSString *)addId addAppName:(NSString *)appName addLanguage:(NSString *)language {
    self.codeId = addId;
    self.appName =
        [appName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    self.domain = domain;
    self.language = language;
}

+ (void)setDomain:(NSString *)ctd {
    domain = ctd;
}

+ (void)setSkipToCustomizePage {
    skipToCustomizePage = TRUE;
}

+ (void)setDesignId:(NSString *)id {
    designId = id;
}

+ (void)setCustomLayout:(CGRect)cl {
    hasCustomLayout = TRUE;
    customLayout = cl;
}

+ (void)removeCustomLayout {
    hasCustomLayout = FALSE;
}

- (CmpConfig *)setTimeout:(long)t {
    timeout = t;
    return self;
}

+ (BOOL)hasCustomLayout {
    return hasCustomLayout;
}

+ (CGRect)getCustomLayout {
    return customLayout;
}

+ (NSString *)domain {
    return domain;
}

+ (void)setCodeId:(NSString *)cti {
    codeId = cti;
}

+ (NSString *)codeId {
    return codeId;
}

+ (void)setAppName:(NSString *)ctan {
    appName =
        [ctan stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

+ (NSString *)appName {
    return appName;
}

+ (void)setLanguage:(NSString *)ctl {
    language = ctl;
}

+ (NSString *)language {
    return language;
}

+ (long)timeout {
    return timeout;
}

+ (BOOL)isValid {
    return domain && appName && codeId && language;
}

+ (void)setIDFA:(NSString *)setIDFA {
    idfa = setIDFA;
}
+ (UIColor *)backgroundColor {
    return backgroundColor;
}
+ (void)setBackgroundColor:(UIColor *)background_color {
    backgroundColor = background_color;
}

+ (long)getTimeout {
    return timeout;
}

+ (void)setAppleTrackingStatus:(NSUInteger)status API_AVAILABLE(ios(14)) {
    attStatus = (NSInteger *)[self getAttStatus:status];
}

+ (void)setAutoAppleTracking:(BOOL)addAttActive {
    attActive = addAttActive;
}

+ (BOOL)getAutoAppleTracking {
    return attActive;
}
+ (NSInteger)getVerboseLevel {
    return verboseLevel;
}
+ (void)setVerboseLevel:(NSInteger)level {
    verboseLevel = level;
}

+ (NSString *)getIdfa {
    if (idfa != nil) {
        return idfa;
    } else {
        return @"";
    }
}

+ (NSString *)getConsentToolURLString:(NSString *)consent {
    [Logger debug:@"Config":[NSString stringWithFormat:@"added Status %@", [NSValue valueWithPointer:attStatus]]];
    if (consent && ![consent containsString:@"null"]) {
        return [NSString stringWithFormat:ConsentToolURLUnformatted, domain, codeId, appName, consent, [CmpConfig getIdfa], language];
    } else {
        return [NSString stringWithFormat:ConsentToolURLUnformatted, domain, codeId, appName, @"", [CmpConfig getIdfa], language];
    }
}
+ (NSInteger)getAttStatus:(NSUInteger)status {
    /**
     * notDetermined = 0
     * restricted = 1
     * denied = 2
     * authorized = 3
     */
    switch (status) {
    case 0:
        return 0;
    case 1:
        return 2;
    case 2:
        return 2;
    case 3:
        return 1;
    default:
        return 0;
    }
}
+ (void)setModalTransitionStyle:(NSInteger)style {
    modalTransitionStyle = style;
}
+ (NSInteger)getModalTransitionStyle {
    return modalTransitionStyle;
}

+ (NSString *)getDesignId {
    return designId;
}

+ (NSInteger)getAppleTrackingStatus {

    return (NSInteger)attStatus;
}

+ (BOOL)getSkipToCustomizePage {
    return (BOOL)skipToCustomizePage;
}

+ (NSString *)description {
    return [NSString stringWithFormat:@"{\r"
                                       "cmpId: %@,\r"
                                       "domain: %@,\r"
                                       "appName: %@,\r"
                                       "idfa: %@,\r"
                                       "isAttActive: %@,\r"
                                       "attStatus: %@,\r"
                                       "}",
                                      codeId, domain, appName, idfa, @(attActive), @((NSInteger)attStatus)];
}

@end
