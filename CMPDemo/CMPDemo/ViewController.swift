//
//  ViewController.swift
//  CMPDemo
//
//  Created by Skander Ben Abdelmalak on 20.11.21.
//

import UIKit
import AppTrackingTransparency
import AdSupport
import CmpSdk

class ViewController: UIViewController {
    
    struct myCmpConfig {
        static let domain = "delivery.consentmanager.net"
        static let appId = ""
        static let appName = "testApp"
        static let language = "DE"
    }
    
    var cmpConsentTool: CMPConsentTool? = nil
    
    func onClose() -> Void {
        NSLog("APP:CLOSED");
    }
    
    func onOpen() -> Void {
        NSLog("APP:OPEN");
        //NSLog(consentTool.getAgreedPurposes())
    }
    
    func onCMPNotOpened() -> Void {
        NSLog("APP:DID NOT OPEN");
    }
    
    func onCMPError(type: CmpErrorType, message: String?) -> Void {
        switch type {
        case .networkError:
            print(message)
            print("error network")
            break
        case .timeoutError:
            print(message)
            print("error timeout")
            break
        case .consentDataReadWriteError:
            print(message)
            print("error consent read/write")
            break
        case .unknownError:
            print(message)
            print("error unknown")
            break
        @unknown default:
            print(message)
            print("error network")
            break
        }
    }
    
    func onButtonClickedEvent(event: CmpButtonEvent) -> Void {
    switch event {
        case .acceptAll:
            print("user accepted all")
            break
        case .rejectAll:
            print("user accepted all")
            break
        case .save:
            print("user saved custom settings")
            break
        case .close:
            print("user closed consent layer without giving consent")
            break
        @unknown default:
            print("unknown button event")
        }
    }
    
    func onClosed() -> Void {
        NSLog("APP:Closed");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let cmpConfig : CmpConfig = CmpConfig.init()
        CmpConfig.setValues(myCmpConfig.domain, addCodeId: myCmpConfig.appId, addAppName: myCmpConfig.appName, addLanguage: myCmpConfig.language)
        //CmpConfig.setVerboseLevel(1);
        CmpConfig.setAppleTrackingStatus(1);
        CmpConfig.setVerboseLevel(4);
        cmpConfig.setTimeout(6000);
        cmpConsentTool = CMPConsentTool.init(
                cmpConfig,
                with: self, addOpenListener: onOpen, addOnCMPNotOpenedListener: onCMPNotOpened).withErrorListener(onCMPError)
        
        
//        cmpConsentTool = CMPConsentTool(cmpConfig: cmpConfig, viewController: self)
//            .withOpenListener(onOpen)
//            .withOnCmpButtonClickedCallback(onButtonClickedEvent)
//            .withOnCMPNotOpenedListener(onCMPNotOpened)
//            .initialize()
//
//        cmpConsentTool = cmpConsentTool?.withOpenListener(onOpen)
//            .withOnCmpButtonClickedCallback(onButtonClickedEvent)
//            .withCloseListener(onClose)
//            .withOnCMPNotOpenedListener(onCMPNotOpened)
        // Add the scroll view to the view hierarchy
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50),
        ])
        
        // Add the stack view to the scroll view
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        scrollView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
        ])
        
        // Create a helper function to add a group of buttons with a separating label
        func addButtons(_ buttons: [(title: String, action: Selector)], groupTitle: String) {
            // Add a label to separate the group
            let groupLabel = UILabel()
            groupLabel.text = groupTitle
            groupLabel.textColor = .darkGray
            groupLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            groupLabel.textAlignment = .center
            stackView.addArrangedSubview(groupLabel)
            
            // Add each button to the stack view
            for buttonData in buttons {
                let button = createButton(title: buttonData.title, action: buttonData.action)
                stackView.addArrangedSubview(button)
            }
        }
        
        // Set up the groups of buttons
        // Loop over the button groups and add the buttons to the stack view
        for group in buttonGroups {
            addButtons(group.buttons, groupTitle: group.title)
        }
        
        // Add the scroll view to the view hierarchy
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }

    // MARK: Initialization Group
    @objc func initializeAction() {
        // action for initialize function
    }
    
    // MARK: Querying Purposes Group
    @objc func getAllPurposesAction() {
        let allPurposes = cmpConsentTool?.getAllPurposes()
        print(allPurposes)
        // action for getAllPurposes function
    }
    
    @objc func getAllPurposeListAction() {
        // action for getAllPurposeList function
        let allPurposes = cmpConsentTool?.getAllPurposesList()
        print(allPurposes)
    }
    
    @objc func getAgreedPurposesAction() {
        let agreedPurposes = cmpConsentTool?.getEnabledPurposes()
        print(agreedPurposes)
        // action for getAgreedPurposes function
    }
    
    @objc func getAgreedPurposeListAction() {
        let agreedPurposes = cmpConsentTool?.getEnabledPurposeList()
        print(agreedPurposes)
        // action for getAgreedPurposeList function
    }
    
    // MARK: Querying Vendors Group
    @objc func getAllVendorsAction() {
        let allVendors = cmpConsentTool?.getAllVendors()
        print(allVendors)
        // action for getAllVendors function
    }
    
    @objc func getAllVendorsListAction() {
        
        // action for getAllVendorsList function
        let allVendors = cmpConsentTool?.getAllVendorList()
        print(allVendors)
    }
    
    @objc func getAgreedVendorsAction() {
        // action for getAgreedVendors function
        let agreedVendors = cmpConsentTool?.getEnabledVendors()
        print(agreedVendors)
    }
    
    @objc func getAgreedVendorListAction() {
        // action for getAgreedVendorList function
        let agreedVendors = cmpConsentTool?.getEnabledVendorList()
        print(agreedVendors)
    }
    
    @objc func checkCmpLayer() {
        cmpConsentTool?.check({() -> Void in
            print("Layer wants to open")
        })
    }
    
    
    // MARK: Querying Consent Status Group
    @objc func calledThisDayAction() {
        // action for calledThisDay function
        let calledToday = cmpConsentTool?.calledThisDay()
        print(calledToday)
    }
    
    @objc func needsAcceptanceAction() {
        let needAcceptance = cmpConsentTool?.needsAcceptance()
        print(needAcceptance)
        // action for needsAcceptance function
        
    }
    
    @objc func hasVendorAction() {
        // action for hasVendor function
        let hasVendor = cmpConsentTool?.hasVendorConsent("", vendorIsV1orV2: true)
        print(hasVendor)
    }
    
    @objc func hasPurposeAction() {
        // action for hasPurpose function
        let hasPurpose = cmpConsentTool?.hasPurposeConsent("", purposeIsV1orV2: true)
        print(hasPurpose)
    }
    
    // MARK: Getting Consent Information Group
    @objc func getUSPrivacyStringAction() {
        let usPrivacy = cmpConsentTool?.getUSPrivacyString()
        print(usPrivacy)
        // action for getUSPrivacyString function
    }
    
    @objc func getGoogleACStringAction() {
        let googleACString = cmpConsentTool?.getGoogleACString()
        print(googleACString)
        // action for getGoogleACString function
    }
    
    @objc func getConsentstring() {
        let consentstring = cmpConsentTool?.getGoogleACString()
        print(consentstring)
        // action for getGoogleACString function
    }
    
    // MARK: Managing Consent Group
    @objc func hasConsentAction() {
        // action for hasConsent function
        let hasConsent = cmpConsentTool?.hasConsent()
        print(hasConsent)
    }
    
    @objc func importConsentStringAction() {
        // action for importConsentString function
    }
    
    @objc func exportConsentStringAction() {
        // action for exportConsentString function
        let exportString = CMPConsentTool.exportCmpString()
        print(exportString)
    }
    
    @objc func enableVendorListAction() {
        let vendors = ["1","2"]
        cmpConsentTool?.enableVendorList(vendors)
        // action for enableVendorList function
    }
    
    @objc func disableVendorListAction() {
        let vendors = ["1","2"]
        cmpConsentTool?.disableVendorList(vendors)
        // action for disableVendorList function
    }
    
    @objc func enablePurposeListAction() {
        // action for enablePurposeList function
        let purposes = ["1","2"]
        cmpConsentTool?.enablePurposeList(purposes)
    }
    
    @objc func disablePurposeListAction() {
        // action for disablePurposeList function
        let purposes = ["1","2"]
        cmpConsentTool?.disablePurposeList(purposes)
    }
    @objc func getDisabledVendorsAction() {
        _ = cmpConsentTool?.getDisabledVendors()
        print("disabled Vendors: ", self.cmpConsentTool?.getDisabledVendors() ?? "")
    }
    @objc func getDisabledPurposesAction() {
        _ = cmpConsentTool?.getDisabledPurposes()
        print("disabled Purposes: ", self.cmpConsentTool?.getDisabledPurposes() ?? "")
    }
    @objc func rejectAllAction() {
        // action for rejectAll function
        cmpConsentTool?.rejectAll({ () -> Void in
            print("enabled Vendors: ", self.cmpConsentTool?.getEnabledVendors() ?? "")
            print("disabled Vendors: ", self.cmpConsentTool?.getDisabledVendors() ?? "")
            print("enabled Purposes", self.cmpConsentTool?.getEnabledPurposeList() ?? "")
            print("disabled Purposes", self.cmpConsentTool?.getDisabledPurposeList() ?? "")
        })
        //print("Export: ", CMPConsentTool.exportCMPData())
    }
    
    @objc func acceptAllAction() {
        cmpConsentTool?.acceptAll({ () -> Void in
            print("enabled Vendors: ", self.cmpConsentTool?.getEnabledVendors() ?? "")
            print("disabled Vendors: ", self.cmpConsentTool?.getDisabledVendors() ?? "")
            print("enabled Purposes", self.cmpConsentTool?.getEnabledPurposeList() ?? "")
            print("disabled Purposes", self.cmpConsentTool?.getDisabledPurposeList() ?? "")
        })
        // action for acceptAll function
    }

    @objc func resetAction()  {
        CMPConsentTool.reset()
    }
    
    // MARK: Displaying Consent UI Group
    @objc func openConsentLayerAction() {
        cmpConsentTool!.openView()
        // action for openConsentLayer function
    }
    
    @objc func checkAndOpenConsentLayerAction() {
        cmpConsentTool?.checkAndOpenConsentLayer()
    }
    
    // Define the button groups as an array of tuples
    let buttonGroups: [(title: String, buttons: [(title: String, action: Selector)])] = [
        ("Displaying Consent UI", [
            ("Open Consent Layer", #selector(openConsentLayerAction)),
            ("Check", #selector(checkCmpLayer)),
            ("Check and Open Consent Layer", #selector(checkAndOpenConsentLayerAction))
            // add other buttons for the displaying consent UI group as needed
        ]),
        ("Managing Consent", [
            ("Has Consent", #selector(hasConsentAction)),
            ("Export Cmp String", #selector(exportConsentStringAction)),
            ("reset Cmp", #selector(resetAction)),
            ("Reject All", #selector(rejectAllAction)),
            ("Accept All", #selector(acceptAllAction)),
            ("Enabling Vendors: ", #selector(enableVendorListAction)),
            ("Disabling Vendors: ", #selector(disableVendorListAction))
            // add other buttons for the managing consent group as needed
        ]),
        
        ("Querying Consent Status", [
            ("Called This Day", #selector(calledThisDayAction)),
            ("need Acceptence", #selector(needsAcceptanceAction)),
            ("has Vendor", #selector(hasVendorAction)),
            ("has Purpose", #selector(hasPurposeAction))
            // add other buttons for the querying consent status group as needed
        ]),
        ("Querying", [
            ("Get All Vendors", #selector(getAllVendorsListAction)),
            ("Get Agreed Vendors", #selector(getAgreedVendorListAction)),
            ("Get disabled Vendors", #selector(getDisabledVendorsAction)),
            ("Get All Purpose List", #selector(getAllPurposeListAction)),
            ("Get Agreed Purpose List", #selector(getAgreedPurposeListAction)),
            ("Get disabled Purposes", #selector(getDisabledPurposesAction)),
            ("Get US Privacy String", #selector(getUSPrivacyStringAction)),
            ("Get Google AC String", #selector(getGoogleACStringAction))
            // add other buttons for the querying vendors group as needed
        ]),
    ]
    func requestPermission() {
           if #available(iOS 14, *) {
               ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                   switch status {
                   case .authorized:
                       // Tracking authorization dialog was shown and accepted
                       // TODO you can now handle your custom code here:
                       print("Authorized %s",status)
                       // Now that we are authorized we can get the IDFA
                       print(ASIdentifierManager.shared().advertisingIdentifier)
                   case .denied:
                       // Tracking authorization dialog was
                       // shown and permission is denied
                       print("Denied %s",status.rawValue)
                   case .notDetermined:
                       // Tracking authorization dialog has not been shown
                       print("Not Determined %s",status.rawValue)
                   case .restricted:
                       print("Restricted %s",status.rawValue)
                   @unknown default:
                       print("Unknown %s",status.rawValue)
                   }
                   CmpConfig.setAppleTrackingStatus(status.rawValue);
               })
           }
       }
    func createButton(title: String, action: Selector) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = UIColor(red: 0.41, green: 0.71, blue: 0.98, alpha: 1.0)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addTarget(self, action: #selector(buttonHighlighted(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(buttonNormal(_:)), for: .touchUpInside)
        return button
    }
    // Define the animation for the button highlight effect
    let highlightDuration = 0.1
    let highlightColor = UIColor(red: 0.41, green: 0.71, blue: 0.98, alpha: 0.7)
    
    // Define the target actions for the highlight and normal animations
    @objc func buttonHighlighted(_ sender: UIButton) {
        UIView.animate(withDuration: highlightDuration) {
            sender.backgroundColor = self.highlightColor
        }
    }
    
    @objc func buttonNormal(_ sender: UIButton) {
        UIView.animate(withDuration: highlightDuration) {
            sender.backgroundColor = UIColor(red: 0.41, green: 0.71, blue: 0.98, alpha: 1.0)
        }
    }
}

